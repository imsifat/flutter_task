import 'package:hive/hive.dart';

part 'repo_search.g.dart';

@HiveType(typeId: 1)
class RepoSearch {
  @HiveField(0)
  String? name;

  @HiveField(1)
  String? owner;

  @HiveField(2)
  String? ownerAvatarUrl;

  @HiveField(3)
  String? description;

  @HiveField(4)
  String? lastUpdated;

  @HiveField(5)
  int? stargazersCount;

  @HiveField(6)
  int? totalCount;

  RepoSearch({
    this.name,
    this.owner,
    this.ownerAvatarUrl,
    this.description,
    this.lastUpdated,
    this.totalCount
  });

  RepoSearch.fromJson(Map<String, dynamic> json) {
    name = json['name'] ?? '';
    owner = json['owner']['login'] ?? '';
    ownerAvatarUrl = json['owner']['avatar_url'] ?? '';
    description = json['description'] ?? '';
    lastUpdated = json['updated_at'] ?? '';
    stargazersCount = json['stargazers_count'] ?? 0;
    totalCount = json['total_count'] ?? 0;
  }
}