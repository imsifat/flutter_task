// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'repo_search.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class RepoSearchAdapter extends TypeAdapter<RepoSearch> {
  @override
  final int typeId = 1;

  @override
  RepoSearch read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return RepoSearch(
      name: fields[0] as String?,
      owner: fields[1] as String?,
      ownerAvatarUrl: fields[2] as String?,
      description: fields[3] as String?,
      lastUpdated: fields[4] as String?,
      totalCount: fields[6] as int?,
    )..stargazersCount = fields[5] as int?;
  }

  @override
  void write(BinaryWriter writer, RepoSearch obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.owner)
      ..writeByte(2)
      ..write(obj.ownerAvatarUrl)
      ..writeByte(3)
      ..write(obj.description)
      ..writeByte(4)
      ..write(obj.lastUpdated)
      ..writeByte(5)
      ..write(obj.stargazersCount)
      ..writeByte(6)
      ..write(obj.totalCount);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RepoSearchAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
