import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../barrel/resources.dart';
import '../localization/app_localization.dart';

class NoDataWidget extends StatelessWidget {

  const NoDataWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [

        const SizedBox(height: 40),

        Center(
          child: Lottie.asset('assets/animations/empty_state.json',
            height: 220,
            width: 220,
            fit: BoxFit.contain,
          ),
        ),

        const SizedBox(height: 10),

        Text(AppLocalization.of(context).getTranslatedValue('no_data_found'),
          textAlign: TextAlign.center,
          style: TextStyles.subtitleStyle,
        ),
      ],
    );
  }
}
