import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../barrel/resources.dart';
import '../barrel/widgets.dart';
import '../localization/app_localization.dart';

class ErrorView extends StatelessWidget {

  final Function() onRetry;

  const ErrorView({Key? key, required this.onRetry}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [

          Lottie.asset('assets/animations/error.json',
            height: 200,
            width: 200,
            fit: BoxFit.fill,
          ),

          const SizedBox(height: 70,),

          Text(AppLocalization.of(context).getTranslatedValue("we_are_sorry"),
            style: TextStyles.subtitleStyle.copyWith(
              fontWeight: FontWeight.w500,
            ),
          ),

          const SizedBox(height: 12,),

          Text(AppLocalization.of(context).getTranslatedValue("an_error_occurred"),
            style: TextStyles.addressStyle.copyWith(
              color: Colors.grey,
            ),
          ),

          const SizedBox(height: 50,),

          MyButton(
            buttonText: AppLocalization.of(context).getTranslatedValue("try_again"),
            onPressed: () {
              onRetry();
            },
          ),
        ],
      ),
    );
  }
}
