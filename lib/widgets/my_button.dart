import 'package:flutter/material.dart';

import '../barrel/resources.dart';

class MyButton extends StatelessWidget {

  final Color backgroundColor;
  final String? buttonText;
  final Function()? onPressed;

  const MyButton({Key? key,
    this.backgroundColor = kSecondaryPrimaryColor,
    required this.buttonText,
    required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return MaterialButton(
      color: backgroundColor,
      splashColor: kAccentColor,
      onPressed: onPressed,
      padding: const EdgeInsets.symmetric(
        vertical: 12,
        horizontal: 25,
      ),
      child: Text(buttonText!.toUpperCase(),
        style: TextStyles.buttonTextStyle,
      ),
    );
  }
}
