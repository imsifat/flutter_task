import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_task/barrel/repository.dart';

import '../../../barrel/contract.dart';
import '../../../barrel/models.dart';
import '../../../barrel/utils.dart';
import '../../../localization/app_localization.dart';

abstract class GitRepoPresenter {
  void getSearchResult(BuildContext context, String query, int? page, int? perPageLimit, String? sort, String? order, {bool? loaderEnable});
}
  class BasicGitRepoPresenter implements GitRepoPresenter {
  BasicGitRepoPresenter({required this.gitRepoContract});

  final GitRepoContract gitRepoContract;
  final _gitRepoRepository = GitRepoRepository();


  @override
  Future<void> getSearchResult(BuildContext context, String query, int? page, int? perPageLimit, String? sort, String? order, {bool? loaderEnable}) async {
    try {
      var response = await _gitRepoRepository.getSearchResult(context, query, page, perPageLimit,sort, order, loaderEnable: loaderEnable);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var data = json.decode(response.body);

        List<RepoSearch> searchResult = [];
        int totalCount;

        try {
          searchResult = List<RepoSearch>.from(data['items'].map((x) => RepoSearch.fromJson(x)));
          totalCount = data['total_count'] ?? 0;
        }
        catch (error) {
          gitRepoContract.onFailed(AppLocalization.of(context).getTranslatedValue("something_went_wrong"));
          return;
        }

        gitRepoContract.showSearchResult(searchResult, totalCount);
        return;
      }

      if (parseError(context, response) != null) {
        gitRepoContract.onFailed(parseError(context, response)!);
        return;
      }

      gitRepoContract.onFailed(AppLocalization.of(context).getTranslatedValue("failed_to_get_data"));
    } on AppException catch (error) {
      gitRepoContract.onFailed(error.message!);
    } catch (error) {
      gitRepoContract.onFailed(AppLocalization.of(context).getTranslatedValue("something_went_wrong"));
    }
  }
}