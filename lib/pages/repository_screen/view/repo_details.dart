import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../barrel/models.dart';
import '../../../barrel/utils.dart';
import '../../../localization/app_localization.dart';

class RepoDetails extends StatelessWidget {
  const RepoDetails({Key? key, required this.repoSearch}) : super(key: key);
  final RepoSearch repoSearch;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(repoSearch.name ?? ''),
      ),
      body: ListView(
        children: [

          Card(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: CachedNetworkImage(
                      imageUrl: repoSearch.ownerAvatarUrl ?? '',
                      height: 100,
                      width: 100,
                      placeholder: (context, url) => const CircularProgressIndicator(),
                      errorWidget: (context, url, error) => const Icon(Icons.error),
                    ),
                  ),

                  const SizedBox(width: 10,),

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(repoSearch.name ?? '', style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20) ),

                      const SizedBox(height: 5,),

                      Card(
                        color: const Color(0xFFC2CBE5),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              const Icon(Icons.star_border),
                              const SizedBox(width: 5,),
                              Text(repoSearch.stargazersCount.toString())
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),

          const SizedBox(height: 10,),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [

                Text('${AppLocalization.of(context).getTranslatedValue('created_by')}: ${repoSearch.owner}', style: const TextStyle(fontWeight: FontWeight.bold,)),

                const SizedBox(height: 10,),

                Text(repoSearch.description ?? ''),

                const SizedBox(height: 10,),
              ],
            ),
          ),

          const Divider(thickness: 1,),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Text('${AppLocalization.of(context).getTranslatedValue('updated_on')}: ${MyDateTime.getYearMonthTime(DateTime.parse(repoSearch.lastUpdated ?? ''))}'),
          ),
        ],
      ),
    );
  }
}
