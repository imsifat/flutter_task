import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:lottie/lottie.dart';

import '../../../barrel/contract.dart';
import '../../../barrel/models.dart';
import '../../../barrel/presenter.dart';
import '../../../barrel/resources.dart';
import '../../../barrel/utils.dart';
import '../../../barrel/widgets.dart';
import '../../../localization/app_localization.dart';
import '../../../route/route_manager.dart';

class GitRepoView extends StatefulWidget {
  const GitRepoView({Key? key}) : super(key: key);

  @override
  State<GitRepoView> createState() => _GitRepoViewState();
}

class _GitRepoViewState extends State<GitRepoView> implements GitRepoContract {
  late final GitRepoPresenter _presenter;
  final ScrollController _scrollController = ScrollController();
  final String _query = 'Flutter';
  String _sort = '', _order = '';
  List<RepoSearch> _repositoryList = [];
  Timer? timer;

  RepoSorted? _repoSorted = RepoSorted.stars;
  RepoOrder? _repoOrder = RepoOrder.descending;

  int _index = 0, _currentPage = 1, _totalItem = 0;
  final _perPageLimit = 10;


  @override
  void initState() {
    super.initState();
    _presenter = BasicGitRepoPresenter(gitRepoContract: this);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _onInit();
      timer = Timer.periodic(const Duration(minutes: 30), (Timer t) => _onInit());
    });
  }

  @override
  Widget build(BuildContext context,) {

    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalization.of(context).getTranslatedValue('app_name')),
        actions: [
          _orderWidget(),
          _sortingWidget(),
        ],
      ),
      body: Center(
        child: IndexedStack(
          index: _index,
          children: [
            Center(
              child: Lottie.asset('assets/animations/circular_loading.json',
                height: 200,
                width: 200,
                fit: BoxFit.fill,
              ),
            ),

            RefreshIndicator(
              onRefresh: () {

                setState(() => _index = 0);
                _onInit(isReload: true);

                return Future(() => null);
              },
              child: _listView(),
            ),

            const NoDataWidget(),

            ErrorView(
              onRetry: () {

                setState(() => _index = 0);
                ScaffoldMessenger.of(context).clearSnackBars();
                _presenter.getSearchResult(context, _query, _currentPage, _perPageLimit, _sort, _order);
              },
            ),
          ],
        )
      ),
    );
  }

  Widget _listView() {
    return ListView(
      controller: _scrollController,
      physics: const AlwaysScrollableScrollPhysics(),
      children: [
        ListView.separated(
          itemCount: _repositoryList.length,
          separatorBuilder: (context, index) => const SizedBox(height: 10,),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () => Navigator.of(context).pushNamed(RouteManager.repoDetails, arguments: _repositoryList[index]),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Container(
                      width: MediaQuery.of(context).size.width,

                      constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                      decoration: const BoxDecoration(
                        color: kPrimaryColor,
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5)),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [

                            Flexible(
                              child: Row(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(7),
                                    child: CachedNetworkImage(
                                      imageUrl: _repositoryList[index].ownerAvatarUrl ?? '',
                                      height: 40,
                                      width: 40,
                                      placeholder: (context, url) => const CircularProgressIndicator(),
                                      errorWidget: (context, url, error) => const Icon(Icons.error),
                                    ),
                                  ),

                                  const SizedBox(width: 10,),

                                  Flexible(child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(_repositoryList[index].name ?? '', style: const TextStyle(color:Colors.white, fontSize: 17, fontWeight: FontWeight.bold),),
                                      const SizedBox(height: 5,),
                                      Text('${AppLocalization.of(context).getTranslatedValue('created_by')}: ${_repositoryList[index].owner}', style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white),),
                                    ],
                                  )),
                                ],
                              ),
                            ),

                            Card(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    const Icon(Icons.star_border),
                                    const SizedBox(width: 5,),
                                    Text(_repositoryList[index].stargazersCount.toString())
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          Text(_repositoryList[index].description ?? ''),

                          const SizedBox(height: 10,),

                          const Divider(thickness: 1,),

                          Text('${AppLocalization.of(context).getTranslatedValue('updated_on')}: ${MyDateTime.getYearMonthTime(DateTime.parse(_repositoryList[index].lastUpdated ?? ''))}'),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          }
        ),

        Visibility(
          visible: _currentPage != 0 && _repositoryList.length < _totalItem,
          child: const Padding(
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: CupertinoActivityIndicator(),
          ),
        ),
      ],
    );
  }

  Widget _sortingWidget() {
    return IconButton(
      tooltip: AppLocalization.of(context).getTranslatedValue('sort'),
      onPressed: () {
        showDialog(context: context, builder: (context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                RadioListTile<RepoSorted>(
                  title: Text(AppLocalization.of(context).getTranslatedValue('most_stars')),
                  value: RepoSorted.stars,
                  groupValue: _repoSorted,
                  onChanged: (RepoSorted? value) {
                    setState(() {
                      _index = 0;
                      _sort = 'stars';
                      _repoSorted = value;
                    });
                    Navigator.pop(context);
                    _presenter.getSearchResult(context, _query, _currentPage, _perPageLimit, _sort, _order);
                  },
                ),
                RadioListTile<RepoSorted>(
                  title: Text(AppLocalization.of(context).getTranslatedValue('recently_updated')),
                  value: RepoSorted.updated,
                  groupValue: _repoSorted,
                  onChanged: (RepoSorted? value) {
                    setState(() {
                      _index = 0;
                      _sort = 'updated';
                      _repoSorted = value;
                      _currentPage = 1;
                    });
                    Navigator.pop(context);
                    _presenter.getSearchResult(context, _query, _currentPage, _perPageLimit, _sort, _order);
                  },
                ),
              ],
            ),
          );
        });
      },
      icon: const Icon(Icons.filter_list),
    );
  }

  Widget _orderWidget() {
    return IconButton(
      tooltip: AppLocalization.of(context).getTranslatedValue('order'),
      onPressed: () {
        showDialog(context: context, builder: (context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                RadioListTile<RepoOrder>(
                  title: Text(AppLocalization.of(context).getTranslatedValue('descending')),
                  value: RepoOrder.descending,
                  groupValue: _repoOrder,
                  onChanged: (RepoOrder? value) {
                    setState(() {
                      _index = 0;
                      _order = 'desc';
                      _repoOrder = value;
                      _currentPage = 1;
                    });
                    Navigator.pop(context);
                    _presenter.getSearchResult(context, _query, _currentPage, _perPageLimit, _sort, _order);
                  },
                ),
                RadioListTile<RepoOrder>(
                  title: Text(AppLocalization.of(context).getTranslatedValue('ascending')),
                  value: RepoOrder.ascending,
                  groupValue: _repoOrder,
                  onChanged: (RepoOrder? value) {
                    setState(() {
                      _index = 0;
                      _order = 'asc';
                      _repoOrder = value;
                    });
                    Navigator.pop(context);
                    _presenter.getSearchResult(context, _query, _currentPage, _perPageLimit, _sort, _order);
                  },
                ),
              ],
            ),
          );
        });
      },
      icon: const Icon(Icons.sort),
    );
  }

  void _onScroll() {

    _scrollController.addListener(() {

      if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent && _repositoryList.length < _totalItem) {
        setState(() {
          _currentPage++;
        });
        _presenter.getSearchResult(context, _query, _currentPage, _perPageLimit, _sort, _order);
      }
    });
  }

  Future<void> _onInit({bool? isReload}) async {
    var result = await Connectivity().checkConnectivity();
    var networkStatus = await InternetConnectionChecker().isHostReachable(options);

    if(isReload != null && isReload) {
      setState(() {
        _index = 0;
      });
    }

    if(result != ConnectivityResult.mobile && result != ConnectivityResult.wifi || !networkStatus.isSuccess) {
      var box = await Hive.openBox(hiveBoxKey);
      final List<RepoSearch> repoList = List<RepoSearch>.from(box.get(repoListKey).map((e) => e));
      _repositoryList = repoList;
      _index = 1;
      setState(() {});
      if(!mounted) return;
      MySnackBar.show(context: context, title: AppLocalization.of(context).getTranslatedValue('error'), message: AppLocalization.of(context).getTranslatedValue('internet_connection_failed'), type: SnackBarType.error);
    }
    else{
      if(!mounted) return;
      _presenter.getSearchResult(context, _query, _currentPage, _perPageLimit, _sort, _order);
      _onScroll();
    }
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  void onFailed(String message, {bool? showErrorPage = true}) {
    if(showErrorPage!) {
      setState(()=> _index = 3);
    }
    MySnackBar.show(context: context, title: AppLocalization.of(context).getTranslatedValue('failed'), message: message, type: SnackBarType.error);
  }

  @override
  void onSuccess(String message) {}

  @override
  Future<void> showSearchResult(List<RepoSearch> repoSearchList, int totalCount) async {
    var box = await Hive.openBox<List<RepoSearch>>(hiveBoxKey);

    if(_currentPage == 1) {
      _repositoryList = repoSearchList;
      _totalItem = totalCount;
      _index = 1;
      box.put(repoListKey, _repositoryList);
    }
    else {
      _repositoryList.addAll(repoSearchList);
      box.put(repoListKey, _repositoryList);
    }

    if(repoSearchList.isNotEmpty) {
      _index = 1;
    }
    else {
      _index = 2;
    }
    setState(() {});
  }
}
