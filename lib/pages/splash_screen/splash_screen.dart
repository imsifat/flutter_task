import 'package:flutter/material.dart';
import 'package:flutter_task/localization/app_localization.dart';

import '../../../barrel/resources.dart';
import '../../route/route_manager.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    _onStart(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            Image.asset('assets/images/icon.png', height: 200,),

            const SizedBox(height: 10,),

            Text(AppLocalization.of(context).getTranslatedValue('app_name'), style: TextStyles.splashScreenTitle,),
          ],
        ),
      ),
    );
  }

  void _onStart(BuildContext context) async {

    await Future<void>.delayed(const Duration(milliseconds: 2500));
    if(!mounted) return;
    Navigator.of(context).pushNamedAndRemoveUntil(RouteManager.repoList, (route) => false);
    return;

  }
}
