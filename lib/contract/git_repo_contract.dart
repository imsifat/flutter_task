import '../barrel/models.dart';

abstract class GitRepoContract {
  void onSuccess(String message);
  void showSearchResult(List<RepoSearch> repoSearchList, int totalCount);
  void onFailed(String message, {bool? showErrorPage = true});
}