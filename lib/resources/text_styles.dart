import 'package:flutter/material.dart';

import '../barrel/resources.dart';

class TextStyles {

  TextStyles._();

  static TextStyle get splashScreenTitle => const TextStyle(
    fontSize: 42,
    color: Color(0xFFA6A6A6),
    fontWeight: FontWeight.bold,
    letterSpacing: 1.0,
  );


  static TextStyle get subtitleStyle => const TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w300,
    color: Colors.black,
  );

  static TextStyle get textFieldHintStyle => TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w500,
    color: Colors.black.withOpacity(.4),
  );

  static TextStyle get buttonTextStyle => const TextStyle(
    fontSize: 16.5,
    fontWeight: FontWeight.w500,
    color: Colors.white,
    letterSpacing: .55,
  );

  static TextStyle get progressIndicatorStyle => const TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w400,
  );

  static TextStyle get snackBarTitleStyle => TextStyle(
    fontSize: 14,
    color: Colors.black.withOpacity(.75),
    fontWeight: FontWeight.w500,
  );

  static TextStyle get snackBarMessageStyle => TextStyle(
    fontSize: 12,
    color: Colors.black.withOpacity(.75),
    fontWeight: FontWeight.w400,
  );

  static TextStyle get appbarTitleStyle => TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w500,
    color: Colors.black.withOpacity(.85),
  );

  static TextStyle get addressStyle => TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w400,
    color: kBackgroundColorTilt,
  );

  static TextStyle get hintStyle => TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w500,
    color: Colors.black.withOpacity(.65),
  );
}