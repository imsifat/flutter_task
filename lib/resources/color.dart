import 'package:flutter/material.dart';

const Color kPrimaryColor = Color(0xFF11608D);
const Color kAccentColor = Color(0xFF1B77AB);
const Color kSecondaryPrimaryColor = Color(0xFF6D6E71);

const Color kBackgroundColorLight = Color(0xFFF2EEEE);
const Color kBackgroundColorDark = Color(0xFF0f0f0f);

Color get kBackgroundColorTilt => kBackgroundColorLight;

const Color kRedAccentColor = Color(0xFFff6958);
const Color kLightSkyBlueColor = Color(0xFFa5d3f2);
const Color kYellowAccentColor = Color(0xFFf8d88c);
const Color kGreenAccentColor = Color(0xFFb0c4a9);
