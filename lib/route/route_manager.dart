import 'package:flutter/material.dart';

import '../barrel/models.dart';
import '../barrel/views.dart';

class RouteManager {

  static const String splashScreen = "/";
  static const String repoList = "/repo-list";
  static const String repoDetails = "/repo-details";

  static Route<dynamic> generate(RouteSettings settings) {
    final dynamic args = settings.arguments;
    switch(settings.name){

      case splashScreen:
        return MaterialPageRoute(builder: (_) => const SplashScreen());

      case repoList:
        return MaterialPageRoute(builder: (_) => const GitRepoView());

      case repoDetails:
        return MaterialPageRoute(builder: (_) => RepoDetails(repoSearch: args as RepoSearch,));

      default:
        return MaterialPageRoute(builder: (_) => const Scaffold(body: SafeArea(child: Center(child: Text("Page not found!")))));
    }
  }
}