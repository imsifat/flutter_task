import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

import '../barrel/utils.dart';
import '../barrel/widgets.dart';
import '../localization/app_localization.dart';

class GitRepoRepository {

  Future<http.Response> getSearchResult(BuildContext context, String query, int? page, int? perPageLimit, String? sort, String? order, {bool? loaderEnable}) async {

    var result = await Connectivity().checkConnectivity();

    if(result != ConnectivityResult.mobile && result != ConnectivityResult.wifi) {
      throw NoInternetException(message: AppLocalization.of(context).getTranslatedValue("no_internet"));
    }

    var networkStatus = await InternetConnectionChecker().isHostReachable(options);

    if(!networkStatus.isSuccess) {
      throw BadConnectionException(message: AppLocalization.of(context).getTranslatedValue("bad_internet"));
    }

    if(loaderEnable != null && loaderEnable) {
      MyProgressIndicator.showTextProgressIndicator(context);
    }

    var client = http.Client();

    String parameter = '?q=$query';

    if(page != null) {
      parameter = '$parameter&page=$page';
    }

    if(perPageLimit != null) {
      parameter = '$parameter&per_page=$perPageLimit';
    }

    if(sort != null) {
      parameter = '$parameter&sort=$sort';
    }

    if(order != null) {
      parameter = '$parameter&order=$order';
    }

    final String url = dotenv.env['BASE_URL']! + dotenv.env['SEARCH_API_URL']! + parameter;

    try {

      var response = await client.get(Uri.parse(url),
        headers: {"Authorization" : "Bearer ${dotenv.env['API_KEY']!}", "Accept": "application/json"},
      ).timeout(const Duration(seconds: timeoutSeconds));

      if(kDebugMode) {
        CustomLogger.debug(trace: CustomTrace(StackTrace.current), tag: "Git Search Result", message: response.body);
      }

      client.close();
      if(loaderEnable != null && loaderEnable) {
        print(loaderEnable);
        MyProgressIndicator.dismiss(context);
      }

      return response;

    } on TimeoutException {

      client.close();

      if(loaderEnable != null && loaderEnable) {
        MyProgressIndicator.dismiss(context);
      }
      throw ConnectionTimedOutException(message: AppLocalization.of(context).getTranslatedValue("timed_out"));

    } catch(error) {

      client.close();

      if(loaderEnable != null && loaderEnable) {
        MyProgressIndicator.dismiss(context);
      }
      throw BadRequestException(message: AppLocalization.of(context).getTranslatedValue("something_went_wrong"));
    }
  }

}