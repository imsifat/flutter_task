import 'dart:io';

import 'package:internet_connection_checker/internet_connection_checker.dart';

const String english = "en";
const String bangla = "bn";

const String hiveBoxKey = "gitRepo";
const String repoListKey = "repoList";

const int kDefaultTimeout = 20;
const int timeoutSeconds = 15;

final AddressCheckOptions options = AddressCheckOptions(address: InternetAddress("8.8.8.8"), port: 53, timeout: const Duration(seconds: 2));

const double kDefaultPaddingHorizontal = 12;
const double kDefaultPaddingVertical = 15;
const double kDefaultRadius = 10;

enum RepoSorted {stars, updated }
enum RepoOrder {ascending, descending }

