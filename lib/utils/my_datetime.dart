import 'package:intl/intl.dart';

class MyDateTime {

  static final DateFormat _yearMonthTime = DateFormat('MM-dd-yyyy hh:mm a');

  static String getYearMonthTime(DateTime dateTime) {
    return _yearMonthTime.format(dateTime);
  }

}