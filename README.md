# flutter_task

This project is a simple project where git repository list shown from github api. You can sort this list by stars, recently updated, ascending and descending order. app screenshot given below. 


<img src="/uploads/5edeaedfe3319a8f9a6c3abf6b9f8c3e/Screenshot_1703148908.png" width="200" height="400">
<img src="/uploads/5dfefbd5f3ba9fec8234c18e980a280e/Screenshot_1703151543.png" width="200" height="400">
<img src="/uploads/c6e4ad72adb0c63d1979a4406cd3dfc6/Screenshot_1703148903.png" width="200" height="400">
